# Notes on Sky QGo assessment - Paul Richards

## Approach to development

I approached this assessment by fully implementing each of the 3 requested features in turn, as they are functionally independent of each other.

The workflow for each feature was as follows;

* Add Redux constant and action
* Implement unit test(s) for the reducer, specifying the expected state after dispatching the new action
* Incorporate the new action into the reducer, modifying the initial state if required
* Connect the relevant actions and state to the React component(s) and pass in as props
* Where appropriate, implement unit test on React component(s) specifying what to render based on state (e.g. if filter is on/off)
* Add DOM elements to the React component

Once all features were added some rudimentary styling was then added.

## What I would change given more time

For the "delete" and "toggle complete" features I would also add some unit tests to check that the relevant action was being called with the correct item ID.

Although this app is very simple in its current form I would also create an "Item" component which would display an individual todo node, and return these in the map in "ItemsList". This would aid reusability and allow flexibility in styling, functionality etc.

The filter feature is implemented in the "ItemCreator" component, which is not a good conceptual fit but was done to save time. It would make sense to have a separate component which handles controls for the list display (filter, sort etc).

In terms of styling I would either create or import custom components for UI rather than using HTML buttons, inputs, etc.