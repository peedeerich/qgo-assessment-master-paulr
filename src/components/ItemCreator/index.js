import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addItem, toggleFilter } from '../../logic/actions';
import './styles.css';

export const ItemCreator = ({ filter, onAdd, onToggleFilter }) => {
  let inputField;

  return (
    <div className={'itemCreator'}>
      <input
        ref={input => {
          inputField = input;
        }}
        className={'itemCreator-input'}
        type="text"
        placeholder={'What do you need to do?'}
      />
      <input
        className={'itemCreator-button'}
        type="button"
        value={'Add Task'}
        onClick={() => {
          inputField.value && onAdd(inputField.value);
          inputField.value = '';
        }}
      />
      <input
        className={'itemCreator-filter'}
        type="button"
        value={filter ? 'Show all' : 'Hide completed'}
        onClick={() => {
          onToggleFilter()
        }}
      />
    </div>
  );
};

ItemCreator.propTypes = {
  filter: PropTypes.bool.isRequired,
  onAdd: PropTypes.func.isRequired,
  onToggleFilter: PropTypes.func.isRequired,
};

const mapStateToProps = state => {
  return { 
    filter: state.todos.filter
  };
};

const mapDispatchToProps = dispatch => ({
  onAdd: newItem => dispatch(addItem(newItem)),
  onToggleFilter: () => dispatch(toggleFilter()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ItemCreator);
