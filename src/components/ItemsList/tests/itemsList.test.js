import React from 'react';
import { shallow } from 'enzyme';
import { ItemsList } from '../index';

const defaultProps = {
  items: [],
  filter: false,
  onDelete: f => f,
  onToggleCompletion: f => f,
  onToggleFilter: () => {},
};

describe('ItemsList', () => {
  it('renders without crashing', () => {
    shallow(<ItemsList {...defaultProps} />);
  });

  it('should display warning message if no items', () => {
    const renderedItem = shallow(<ItemsList {...defaultProps} items={[]} />);
    expect(renderedItem.find('#items-missing')).toHaveLength(1);
  });

  it('should not display warning message if items are present', () => {
    const items = [{ id: 1, content: 'Test 1' }];
    const renderedItem = shallow(<ItemsList {...defaultProps} items={items} />);
    expect(renderedItem.find('#items-missing')).toHaveLength(0);
  });

  it('should render items as list items', () => {
    const items = [{ id: 1, content: 'Test 1' }, { id: 2, content: 'Test 2' }];
    const renderedItem = shallow(<ItemsList {...defaultProps} items={items} />);
    expect(renderedItem.find('li')).toHaveLength(2);
  });

  it('should render all items if filter is inactive', () => {
    const items = [{ id: 1, content: 'Test 1', complete: true }, { id: 2, content: 'Test 2', complete: false }];
    const filter = false;
    const renderedItem = shallow(<ItemsList {...defaultProps} items={items} filter={filter}/>)
    expect(renderedItem.find('li')).toHaveLength(2);
  });

  it('should render only incomplete items if filter is active', () => {
    const items = [{ id: 1, content: 'Test 1', complete: true }, { id: 2, content: 'Test 2', complete: false }];
    const filter = true;
    const renderedItem = shallow(<ItemsList {...defaultProps} items={items} filter={filter}/>)
    expect(renderedItem.find('li')).toHaveLength(1);
    expect(renderedItem.find('.itemsList-item-complete')).toHaveLength(0);
  });
});
