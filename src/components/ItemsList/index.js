import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { deleteItem, toggleCompletion } from '../../logic/actions';
import './styles.css';

export const ItemsList = ({ items, filter, onDelete, onToggleCompletion }) => {
  return (
    <div>
      <ul className={'itemsList-ul'}>
        {items.length < 1 && <p id={'items-missing'}>Add some tasks above.</p>}
        {items.filter(item => (!(filter && item.complete))).map(item => <li key={item.id}>
          <div className={item.complete ? 'itemsList-item-complete' : 'itemsList-item-incomplete'}>
            {item.content}
            <input
              className={'itemsList-item-toggleCompletion'}
              type={'button'}
              value={item.complete ? 'Mark incomplete' : 'Mark complete'}
              onClick={() => {
                onToggleCompletion(item.id)
              }}
            />
            <input 
              className={'itemsList-item-delete'}
              type={'button'}
              value={'Delete item'}
              onClick={() => {
                onDelete(item.id)
              }}
            />
          </div>
        </li>)}
      </ul>
    </div>
  );
};

ItemsList.propTypes = {
  items: PropTypes.array.isRequired,
  onDelete: PropTypes.func.isRequired,
  onToggleCompletion: PropTypes.func.isRequired,
};

const mapStateToProps = state => {
  return {
   items: state.todos.items,
   filter: state.todos.filter,
  };
};

const mapDispatchToProps = dispatch => ({
  onDelete: itemId => dispatch(deleteItem(itemId)),
  onToggleCompletion: itemId => dispatch(toggleCompletion(itemId)),
})

export default connect(mapStateToProps, mapDispatchToProps)(ItemsList);
