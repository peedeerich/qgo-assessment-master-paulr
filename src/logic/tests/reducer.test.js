import reducer, { initialState } from '../reducer';
import { addItem, deleteItem, toggleCompletion, toggleFilter } from '../actions';

describe('reducer', () => {
  it('should return state for unknown action', () => {
    const mockState = { test: 'testItem' };
    const mockAction = { type: 'mystery-meat' };
    const result = reducer(mockState, mockAction);
    expect(result).toEqual(mockState);
  });

  it('should use initial state if state not provided', () => {
    const mockAction = { type: 'mystery-meat' };
    const result = reducer(undefined, mockAction);
    expect(result).toEqual(initialState);
  });

  it('should add new items on ADD_ITEM', () => {
    const state = {
      items: [
        { id: 1, content: 'first', complete: false },
        { id: 2, content: 'second', complete: false },
      ]
    }
    const mockAction = addItem('third');
    const result = reducer(state, mockAction);
    expect(result.items).toHaveLength(3);
    expect(result.items[2].id).toEqual(3);
    expect(result.items[2].content).toEqual('third');
  });

  it('should delete an item on DELETE_ITEM', () => {
    const state = {
      items: [
        { id: 1, content: 'first', complete: false },
        { id: 2, content: 'second', complete: false },
      ]
    }
    const mockAction = deleteItem(1);
    const result = reducer(state, mockAction);
    expect(result.items).toHaveLength(1);
    expect(result.items[0].id).toEqual(2);
    expect(result.items[0].content).toEqual('second');
  });

  it('should toggle complete for an incomplete task on TOGGLE_COMPLETION', () => {
    const state = {
      items: [
        { id: 1, content: 'first', complete: false },
        { id: 2, content: 'second', complete: true },
      ]
    }
    const mockAction = toggleCompletion(1);
    const result = reducer(state, mockAction);
    expect(result.items[0].complete).toEqual(true);
  });

  it('should toggle complete for a completed task on TOGGLE_COMPLETION', () =>{
    const state = {
      items: [
        { id: 1, content: 'first', complete: false },
        { id: 2, content: 'second', complete: true },
      ]
    }
    const mockAction = toggleCompletion(2);
    const result = reducer(state, mockAction);
    expect(result.items[1].complete).toEqual(false);
  });

  it('should activate filter on TOGGLE_FILTER', () => {
    const state ={
      filter: false
    }
    const mockAction = toggleFilter();
    const result = reducer(state, mockAction);
    expect(result.filter).toEqual(true);
  });

  it('should deactivate filter on TOGGLE_FILTER', () => {
    const state ={
      filter: true
    }
    const mockAction = toggleFilter();
    const result = reducer(state, mockAction);
    expect(result.filter).toEqual(false);
  });
});
