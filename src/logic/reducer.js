import { ADD_ITEM, DELETE_ITEM, TOGGLE_COMPLETION, TOGGLE_FILTER } from './constants';

export const initialState = {
  items: [
    { id: 1, content: 'Call mum', complete: false },
    { id: 2, content: 'Buy cat food', complete: false },
    { id: 3, content: 'Water the plants', complete: false },
  ],
  filter: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_ITEM:
      const nextId =
        state.items.reduce((id, item) => Math.max(item.id, id), 0) + 1;
      const newItem = {
        id: nextId,
        content: action.content,
      };

      return {
        ...state,
        items: [...state.items, newItem],
      };
    case DELETE_ITEM:
      return {
        ...state,
        items: [...state.items.filter(item => item.id !== action.itemId)]
      };
    case TOGGLE_COMPLETION:
      const oldItem = state.items.find(item => item.id === action.itemId);
      const toggledItem = {
        ...oldItem,
        complete: !oldItem.complete
      };
      return {
        ...state,
        items: [...state.items.filter(item => item.id !== action.itemId), toggledItem].sort((itemA, itemB) => itemA.id > itemB.id)
      };
    case TOGGLE_FILTER:
      return {
        ...state,
        filter: !state.filter
      };
    default:
      return state;
  }
};

export default reducer;
