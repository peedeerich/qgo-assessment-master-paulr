export const ADD_ITEM = 'qgo/assessment/ADD_ITEM';
export const DELETE_ITEM = 'qgo/assessment/DELETE_ITEM';
export const TOGGLE_COMPLETION = 'qgo/assessment/TOGGLE_COMPLETION';
export const TOGGLE_FILTER = 'qgo/assessment/TOGGLE_FILTER';
