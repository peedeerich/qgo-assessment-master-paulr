import { ADD_ITEM, DELETE_ITEM, TOGGLE_COMPLETION, TOGGLE_FILTER } from './constants';

export const addItem = content => {
  return { type: ADD_ITEM, content };
};

export const deleteItem = itemId => {
  return { type: DELETE_ITEM, itemId };
};

export const toggleCompletion = itemId => {
  return { type: TOGGLE_COMPLETION, itemId };
};

export const toggleFilter = () => {
  return { type: TOGGLE_FILTER };
};